#!/usr/bin/env python3

import argparse
import datetime
from distutils.version import StrictVersion
import fileinput
import pathlib
import subprocess


def update_version(version: str) -> None:
    p = pathlib.Path('config.yml')

    with fileinput.input(p, inplace=True) as f:
        def replace_version(line: str) -> str:
            if 'LatestVersion:' in line:
                # Avoid overwriting 20.1.0 when releasing 20.0.8
                old_version = line.split(':')[1].strip()
                if StrictVersion(old_version) < StrictVersion(version):
                    return f'  LatestVersion: {version}\n'
            return line
        for line in f:
            print(replace_version(line).rstrip())

    subprocess.run(['git', 'add', p])


def post_news(version: str) -> None:
    version_slug = version.replace('.', '-')
    p = pathlib.Path(f'content/news/releases/mesa-{version_slug}-is-released.md')

    now = datetime.datetime.now()

    with open(p, 'w') as f:
        f.write('---\n')
        f.write(f'title:    "Mesa {version} is released"\n')
        f.write(f'date:     {now.strftime("%Y-%m-%d %H:%M:%S")}\n')
        f.write('category: releases\n')
        f.write('tags:     []\n')
        f.write('---\n')
        f.write(f'[Mesa {version}](https://docs.mesa3d.org/relnotes/{version}.html) is released. ')
        if version.endswith('.0'):
            f.write('This is a new development release. ')
            f.write('See the release notes for more information about this release.')
        else:
            f.write('This is a bug fix release.')
        f.write('\n')

    subprocess.run(['git', 'add', p])


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('version', help='The released version.')
    args = parser.parse_args()

    # Don't post release candidates to the homepage
    if 'rc' in args.version:
        return

    update_version(args.version)
    post_news(args.version)

    subprocess.run(['git', 'commit', '-m',
                    f'release {args.version}'])


if __name__ == '__main__':
    main()
