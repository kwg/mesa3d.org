---
title:    "August 14, 1999"
date:     1999-08-14 00:00:00
category: misc
tags:     []
summary:  "[www.mesa3d.org](https://www.mesa3d.org) is having technical problems
due to hardware failures at VA Linux systems. The Mac pages, ftp, and
CVS services aren't fully restored yet. Please be patient."
---
[www.mesa3d.org](https://www.mesa3d.org) is having technical problems
due to hardware failures at VA Linux systems. The Mac pages, ftp, and
CVS services aren't fully restored yet. Please be patient.

\-Brian
