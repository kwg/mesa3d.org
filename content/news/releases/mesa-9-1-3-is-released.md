---
title:    "Mesa 9.1.3 is released"
date:     2013-05-21 00:00:00
category: releases
tags:     []
---
[Mesa 9.1.3](https://docs.mesa3d.org/relnotes/9.1.3.html) is released. This is a bug fix
release.
