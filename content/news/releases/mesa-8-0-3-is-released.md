---
title:    "Mesa 8.0.3 is released"
date:     2012-05-18 00:00:00
category: releases
tags:     []
---
[Mesa 8.0.3](https://docs.mesa3d.org/relnotes/8.0.3.html) is released. This is a bug fix
release.
