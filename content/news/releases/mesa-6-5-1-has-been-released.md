---
title:    "Mesa 6.5.1 has been released"
date:     2006-09-15 00:00:00
category: releases
tags:     []
---
[Mesa 6.5.1](https://docs.mesa3d.org/relnotes/6.5.1.html) has been released. This is a new
development release.
