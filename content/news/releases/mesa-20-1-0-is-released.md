---
title:    "Mesa 20.1.0 is released"
date:     2020-05-27 00:00:00
category: releases
tags:     []
---
[Mesa 20.1.0](https://docs.mesa3d.org/relnotes/20.1.0.html) is released. This is a new
development release. See the release notes for more information about
this release.
