---
title:    "Mesa 21.0 is released"
date:     2021-03-11 16:45:24
category: releases
tags:     []
---
[Mesa 21.0](https://docs.mesa3d.org/relnotes/21.0.html) is released. This is a new development release. See the release notes for more information about this release.
