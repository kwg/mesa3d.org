---
title:    "Mesa 20.0.0 is released"
date:     2020-02-19 00:00:00
category: releases
tags:     []
---
[Mesa 20.0.0](https://docs.mesa3d.org/relnotes/20.0.0.html) is released. This is a new
development release. See the release notes for more information about
this release.
