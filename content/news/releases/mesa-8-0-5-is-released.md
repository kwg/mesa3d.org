---
title:    "Mesa 8.0.5 is released"
date:     2012-10-24 00:00:00
category: releases
tags:     []
---
[Mesa 8.0.5](https://docs.mesa3d.org/relnotes/8.0.5.html) is released. This is a bug fix
release.
