---
title:    "Mesa 20.1.2 is released"
date:     2020-06-24 23:08:28
category: releases
tags:     []
---
[Mesa 20.1.2](https://docs.mesa3d.org/relnotes/20.1.2.html) is released. This is a bug fix release.
