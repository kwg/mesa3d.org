---
title:    "Mesa 18.2.5 is released"
date:     2018-11-15 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.5](https://docs.mesa3d.org/relnotes/18.2.5.html) is released. This is a bug-fix
release.
